# Postgres Settings

- DB_HOST
- DB_PORT
- DB_NAME
- DB_USER
- DB_PASS

# FTP Settings

- FTP_HOST
- FTP_USER
- FTP_PASS
- FTP_PATH

# Cron Settings
- BACKUP_PATH
- CRON_RULES