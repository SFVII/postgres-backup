#!/bin/bash

envsubst < erontab_default > /etc/cron.d/backup-cron
chmod 0644 /etc/cron.d/backup-cron

sh -c crond && tail -f /var/log/cron.log