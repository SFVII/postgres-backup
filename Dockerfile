FROM alpine:latest

RUN apk --no-cache add postgresql-client lftp bash

COPY backup-script.sh /backup-script.sh

RUN chmod +x /backup-script.sh

COPY crontab /erontab_default

RUN touch /var/log/cron.log

ENV CRON_RULES="0 1 * * *"

ENV BACKUP_PATH=/tmp

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x  /entrypoint.sh

CMD ["bash", "entrypoint.sh"]