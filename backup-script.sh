#!/bin/bash

if [ -z "$DB_HOST" ] || [ -z "$DB_PORT" ] || [ -z "$DB_NAME" ] || [ -z "$DB_USER" ] || [ -z "$DB_PASS" ]; then
    echo "Error Missing configuration postgres all postgres variable are requested"
    exit -1
elif [ -z "$FTP_HOST" ] || [ -z "$FTP_USER" ] || [ -z "$FTP_PASS" ] || [ -z "$FTP_PATH" ]; then
    echo "Error Missing configuration FTPserver all FTPserver variable are requested"
    exit -1
else
    # Timestamp for backup filename
    TIMESTAMP=$(date +%Y%m%d%H%M%S)

    # Perform PostgreSQL backup
    pg_dump -h $DB_HOST -p $DB_PORT -U $DB_USER -d $DB_NAME | gzip > "$BACKUP_PATH/$DB_NAME-backup-$TIMESTAMP.sql.gz"

    # Upload backup to FTP
    ftp -n $FTP_HOST <<END_SCRIPT
    quote USER $FTP_USER
    quote PASS $FTP_PASS
    binary
    cd $FTP_PATH
    put "$BACKUP_PATH/$DB_NAME-backup-$TIMESTAMP.sql.gz"
    quit
    END_SCRIPT

    # Check if FTP upload was successful
    if [ $? -eq 0 ]; then
        echo "Backup uploaded to FTP server."
    else
        echo "Backup upload to FTP server failed."
    fi
fi
